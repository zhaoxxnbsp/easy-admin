package ${mapperPkg};

import com.mybatisflex.core.BaseMapper;
import ${poPkg}.${poClassName};

/**
* ${tableComment} Mapper 接口
*
* @author ${author}
* @since ${datetime}
*/
public interface ${mapperClassName} extends BaseMapper<${className}> {

}
