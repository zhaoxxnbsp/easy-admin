package com.mars.easy.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyAdminBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasyAdminBootApplication.class, args);
    }

}
