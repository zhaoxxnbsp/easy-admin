package com.mars.easy.admin.modules.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.mars.easy.admin.modules.system.pojo.po.SysClient;


/**
* <p>
* 系统授权表 Mapper 接口
* </p>
*
* @Author 程序员Mars
* @since 2024-01-22
*/
public interface SysClientMapper extends BaseMapper<SysClient> {

}
