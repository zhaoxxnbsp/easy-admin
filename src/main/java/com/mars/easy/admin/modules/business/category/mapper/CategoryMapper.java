package com.mars.easy.admin.modules.business.category.mapper;

import com.mybatisflex.core.BaseMapper;
import com.mars.easy.admin.modules.business.category.pojo.po.Category;

/**
* 菜品及套餐分类 Mapper 接口
*
* @author Mars
* @since 2024-08-22
*/
public interface CategoryMapper extends BaseMapper<Category> {

}
