package com.mars.easy.admin.modules.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.mars.easy.admin.modules.system.pojo.po.SysDataRoleMenu;


/**
* <p>
* 系统数据角色-菜单表 Mapper 接口
* </p>
*
* @Author 程序员Mars-admin
* @since 2024-07-11
*/
public interface SysDataRoleMenuMapper extends BaseMapper<SysDataRoleMenu> {

}
