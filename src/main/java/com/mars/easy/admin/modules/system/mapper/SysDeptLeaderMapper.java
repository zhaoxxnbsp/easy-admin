package com.mars.easy.admin.modules.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.mars.easy.admin.modules.system.pojo.po.SysDeptLeader;


/**
* <p>
* 部门领导人表 Mapper 接口
* </p>
*
* @Author 程序员Mars
* @since 2024-03-26
*/
public interface SysDeptLeaderMapper extends BaseMapper<SysDeptLeader> {

}
