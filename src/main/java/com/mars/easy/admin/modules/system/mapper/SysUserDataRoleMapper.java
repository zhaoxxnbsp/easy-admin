package com.mars.easy.admin.modules.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.mars.easy.admin.modules.system.pojo.po.SysUserDataRole;


/**
* <p>
* 系统用户-数据角色关联表 Mapper 接口
* </p>
*
* @Author 程序员Mars-admin
* @since 2024-07-11
*/
public interface SysUserDataRoleMapper extends BaseMapper<SysUserDataRole> {

}
