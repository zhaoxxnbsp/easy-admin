package com.mars.easy.admin.modules.system.pojo.dto.sysclient;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
/**
 * <p>
 * SysClient导入DTO
 * </p>
 *
 * @Author 程序员Mars
 * @since 2024-01-22
 */
@Data
@Schema(description = "SysClient导入DTO")
public class SysClientImportDTO {


}
