package com.mars.easy.admin.modules.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.mars.easy.admin.modules.system.pojo.po.SysDataRole;


/**
* <p>
* 数据权限管理 Mapper 接口
* </p>
*
* @Author 程序员Mars-admin
* @since 2024-07-09
*/
public interface SysDataRoleMapper extends BaseMapper<SysDataRole> {

}
