package com.mars.easy.admin.modules.system.mapper;


import com.mybatisflex.core.BaseMapper;
import com.mars.easy.admin.modules.system.pojo.po.SysConfig;

/**
 * <p>
 * 参数配置表 Mapper 接口
 * </p>
 *
 * @Author 程序员Mars
 * @since 2023-11-23
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
