package com.mars.easy.admin.modules.business.addressbook.mapper;

import com.mybatisflex.core.BaseMapper;
import com.mars.easy.admin.modules.business.addressbook.pojo.po.AddressBook;

/**
* 地址管理 Mapper 接口
*
* @author Mars
* @since 2024-08-22
*/
public interface AddressBookMapper extends BaseMapper<AddressBook> {

}
