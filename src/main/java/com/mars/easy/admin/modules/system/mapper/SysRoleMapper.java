package com.mars.easy.admin.modules.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.mars.easy.admin.modules.system.pojo.po.SysRole;

/**
 * <p>
 * 系统角色表 Mapper 接口
 * </p>
 *
 * @Author 程序员Mars
 * @since 2023-08-21
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
