package com.mars.easy.admin.modules.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.mars.easy.admin.modules.system.pojo.po.SysUserDept;


/**
* <p>
* 用户-部门关系表 Mapper 接口
* </p>
*
* @Author 程序员Mars
* @since 2024-04-02
*/
public interface SysUserDeptMapper extends BaseMapper<SysUserDept> {

}
