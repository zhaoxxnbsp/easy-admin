package com.mars.easy.admin.modules.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.mars.easy.admin.modules.system.pojo.po.SysFile;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @Author 程序员Mars
 * @since 2023-08-31
 */
public interface CommonFileMapper extends BaseMapper<SysFile> {

}
