package com.mars.easy.admin.modules.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.mars.easy.admin.modules.system.pojo.po.SysDataRoleRelation;


/**
* <p>
* 系统数据角色-关联表 Mapper 接口
* </p>
*
* @Author 程序员Mars-admin
* @since 2024-07-11
*/
public interface SysDataRoleRelationMapper extends BaseMapper<SysDataRoleRelation> {

}
