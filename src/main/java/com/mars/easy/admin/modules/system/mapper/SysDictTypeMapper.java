package com.mars.easy.admin.modules.system.mapper;

import com.mybatisflex.core.BaseMapper;
import com.mars.easy.admin.modules.system.pojo.po.SysDictType;

/**
 * <p>
 * 字典类型 Mapper 接口
 * </p>
 *
 * @Author 程序员Mars
 * @since 2023-08-18
 */
public interface SysDictTypeMapper extends BaseMapper<SysDictType> {


}
