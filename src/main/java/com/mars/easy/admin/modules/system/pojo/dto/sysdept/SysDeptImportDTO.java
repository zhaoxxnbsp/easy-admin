package com.mars.easy.admin.modules.system.pojo.dto.sysdept;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * <p>
 * SysDept导入DTO
 * </p>
 *
 * @Author 程序员Mars
 * @since 2024-03-20
 */
@Data
@Schema(description = "SysDept导入DTO")
public class SysDeptImportDTO {


}
