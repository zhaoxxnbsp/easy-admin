package com.mars.easy.admin.framework.enums;

/**
 * @ClassName CommonTrueFalseEnum
 * @Author 程序员Mars
 * @Date 2024/1/10 9:12
 * @Version 1.0
 */
public enum TrueFalseEnum {
    T,
    F
}
