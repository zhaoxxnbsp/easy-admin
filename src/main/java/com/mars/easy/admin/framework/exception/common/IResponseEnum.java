package com.mars.easy.admin.framework.exception.common;

public interface IResponseEnum {

    int getCode();

    String getMessage();

}
