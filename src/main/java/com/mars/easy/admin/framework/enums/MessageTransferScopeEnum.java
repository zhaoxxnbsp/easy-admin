package com.mars.easy.admin.framework.enums;

/**
 * @ClassName MessageTransferScopeEnum
 * @Author 程序员Mars
 * @Date 2024/2/17 8:48
 * @Version 1.0
 */
public enum MessageTransferScopeEnum {
    // 后台服务端
    SERVER,
    // socket服务端
    SOCKET_SERVER,
    // socket客户端 （web、app等）
    SOCKET_CLIENT,

}
